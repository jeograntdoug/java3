package day09todosdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

public class Database {
    private Connection dbConn;
    
    public Database() throws SQLException {
        
        dbConn = DriverManager.getConnection("jdbc:mysql://localhost/ipd20todos?"
                + "user=heok&password=lala");
    }
    
    public ArrayList<Todo> getTodos() throws SQLException{
        ArrayList<Todo> todoList = new ArrayList<>();
        
        PreparedStatement preStmt = dbConn.prepareStatement("SELECT * FROM todos");
        ResultSet resultSet = preStmt.executeQuery();
        
        while(resultSet.next()){
            try {
                int id =resultSet.getInt("id");
                String task = resultSet.getString("task");
                int difficulty = resultSet.getInt("difficulty");
                Date date = resultSet.getDate("dueDate");
                String status = resultSet.getString("status");

                Todo newTodo = new Todo(id,task,difficulty,date,status);
                todoList.add(newTodo);
            }catch(InvalidDataException ex){
                System.out.println("ERROR DATABASE:" + ex.getMessage());
            }
        }
        
        return todoList;
    }
    
    public void addTodo(Todo todo) throws SQLException {
        PreparedStatement preStmt = dbConn.prepareStatement("INSERT INTO todos VALUES (NULL,?,?,?,?)");
        //id,task,difficulty,date,status
        preStmt.setString(1,todo.getTask());
        preStmt.setInt(2,todo.getDifficulty());
        preStmt.setDate(3, java.sql.Date.valueOf(todo.getDueDate()));
        preStmt.setString(4, todo.getStatus().toString());
        
        preStmt.executeUpdate();
    }
    
    public void updateTodo(Todo todo) throws SQLException {
        PreparedStatement preStmt 
                = dbConn.prepareStatement("UPDATE todos "
                        + "SET task=?,difficulty=?,dueDate=?,status=?"
                        + "WHERE id=?"
                );
        
        preStmt.setInt(1, todo.getId());
        preStmt.setString(2,todo.getTask());
        preStmt.setInt(3,todo.getDifficulty());
        preStmt.setDate(4, java.sql.Date.valueOf(todo.getDueDate()));
        preStmt.setString(5, todo.getStatus().toString());
        
        preStmt.executeUpdate();
    }
    
    public void deleteTodo(int id) throws SQLException {
        PreparedStatement preStmt = dbConn.prepareStatement("DELETE FROM todos WHERE id=?");
        preStmt.setInt(1, id);
        
        preStmt.executeUpdate();
    }
    
    public int getIdForNewTodo() throws SQLException{
        PreparedStatement preStmt = dbConn.prepareStatement("SELECT max(id) FROM todos");
        ResultSet resultSet = preStmt.executeQuery();
        resultSet.next();
                
        return resultSet.getInt(1) + 1;
    }
}
 