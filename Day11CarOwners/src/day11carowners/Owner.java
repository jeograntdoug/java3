
package day11carowners;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;
import javax.imageio.ImageIO;

class InvalidDataException extends Exception {
    public InvalidDataException(String msg){
        super(msg);
    }
}

public class Owner {
    // For database input
    public Owner(int id, String name, Blob photo,int carsOwned) throws InvalidDataException{
        setId(id);
        setName(name);
        setPhoto(photo);
        setCarsOwned(carsOwned);
    }
    
    // For user input
    public Owner(int id,String name, BufferedImage photo) throws InvalidDataException{
        setId(id);
        setName(name);
        setPhoto(photo);
    }
    
    private int id;
    private String name;
    private BufferedImage photo;
    
    private int carsOwned;
    
    public int getId(){ return this.id; }
    private void setId(int id) { this.id = id; }
    
    public String getName() { return this.name; }
    private void setName(String name) throws InvalidDataException{
        if(name == null){
            throw new InvalidDataException("Name must be not null");
        }
        if(name.isEmpty() || name.length()>100){
            throw new InvalidDataException("Name must be 1~100" + name);
        }
        this.name = name;
    }
    
    public BufferedImage getPhoto() { return this.photo; }
    private void setPhoto(BufferedImage photo){ this.photo = photo; }
    private void setPhoto(Blob photo) throws InvalidDataException{
        if(photo == null){ 
            this.photo = null; 
            return;
        }
        try{
            BufferedImage buf = ImageIO.read(photo.getBinaryStream());
            setPhoto(buf);
        }catch (IOException | SQLException  ex){
            throw new InvalidDataException("Cannot convert byte[] photo to Bufferedimage");
        }
    }
    
    private void setCarsOwned(int carsOwned){ this.carsOwned = carsOwned; }
    
    @Override
    public String toString(){
        return String.format("%d:%s owns %s cars",id,name,carsOwned);
    }
}
