/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day03person;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author 1898918
 */
public class Person {
    //TODO : add getters and setters
    String name; // 1=100 characters, excluding;
    int heightCm; // 0-250 cm
    Date dateOfBirth;
    
    public Person (String name, int heightCm, String dateOfBirth)
    throws PersonDataException 
    {
        setName(name);
        setHeightCm(heightCm);
        setDateOfBirth(dateOfBirth);
    }
    
    public void setName(String name) throws PersonDataException{
        if(!name.matches("[a-zA-Z ]{1,50}")){
            throw new PersonDataException("Invalid Name: [" + name + "]");
        }
        this.name = name;
    }
    
    public void setHeightCm(int heightCm) throws PersonDataException{
        if(heightCm < 100 || heightCm > 250){
            throw new PersonDataException("Invalid Height: [" + heightCm + "]");
        }
        this.heightCm = heightCm;
    }
    
    public void setDateOfBirth(String dateOfBirthStr) throws PersonDataException{
        try {
            Date date = dateFormat.parse(dateOfBirthStr);
            Calendar.Builder calBuilder = new Calendar.Builder();
            calBuilder.setInstant(date);
            Calendar cal = calBuilder.build();
            int year = cal.get(Calendar.YEAR);
            if(year > 2100 || 1900 > year){
                throw new PersonDataException("Invalid Date: [" + dateOfBirthStr + "]\n"
                + "Birth Year must be 1900~2100");
            }
            
            this.dateOfBirth = date;
        } catch (ParseException ex) {
            throw new PersonDataException("Invalid Date: [" + dateOfBirthStr + "]");
        }
    }
    
    public String getName(){ return name; }
    public int getHeightCm(){ return heightCm; }
    public String getDate(){ return dateFormat.format(dateOfBirth); }
    
    @Override
    public String toString() {
        return String.format("%s is %dcm tall born %s",
                name, heightCm, dateFormat.format(dateOfBirth));
        
    }
    
    public boolean matches(String pattern){
        // filter is empty
        if(pattern == null) { return true; }
        
        Pattern p = Pattern.compile(pattern);
        
        Matcher nameMatcher = p.matcher(name);
        if(nameMatcher.find()) { return true; }
         
        Matcher heightMatcher = p.matcher(String.valueOf(heightCm));
        if(heightMatcher.find()) { return true; }
         
        Matcher dateMatcher = p.matcher(dateFormat.format(dateOfBirth));
        if(dateMatcher.find()) { return true; } 
         
        return false;
    }
    
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
}
