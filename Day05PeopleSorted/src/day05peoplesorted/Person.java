package day05peoplesorted;

import java.text.ParseException;
import java.util.Comparator;

public class Person {

    private String name;
    private int age;
    private double weight;
    
    Person (String name, int age, double weight){
        setName(name);
        setAge(age);
        setWeight(weight);
    }
    Person (String name, String age, String weight){
        setName(name);
        setAge(age);
        setWeight(weight);
    }
    Person (String dataLine){
        String[] data = dataLine.split(";");
        
        if(data.length != 3){
            throw new DataInvalidException("Data format is wrong");
        }
        
        setName(data[0]);
        setAge(data[1]);
        setWeight(data[2]);
    }

    public String getName() { return name; }
    public int getAge() { return age; }
    public double getWeight() { return weight; }

    public void setName(String name) { 
        if(!isValidName(name)){ 
            throw new DataInvalidException("Name must be 1~50 charactors.");
        }
        this.name = name; 
    }
    
    public void setAge(int age) { 
        if(!isValidAge(age)){ 
            throw new DataInvalidException("Age must be 0~150");
        }
        this.age = age; 
    }
    
    public void setAge(String age)
    throws DataInvalidException {
        try {
            setAge(Integer.parseInt(age));
        } catch( NumberFormatException ex){
            throw new DataInvalidException("Age has to be integer");
        }
    }
    
    public void setWeight(double weight) { 
        if(!isValidWeight(weight)) { 
            throw new DataInvalidException("Weight has to be 0~300");
        }
        this.weight = weight; 
    }
    public void setWeight(String weight){
        try{
            Person.this.setWeight(Double.parseDouble(weight));
        } catch (NumberFormatException ex){
            throw new DataInvalidException("Weight has to be double");
        }
    }
    
    public static boolean isValidName(String name){
        if(name == null) { return false; }
        
        if(!name.matches("[a-zA-Z0-9 _()-]{1,50}")){
            return false;
        }
        return true;
    }
    
    public static boolean isValidAge(int age){
        if(age > 150 || 1 > age){
            return false;
        }
        return true;
    }
    public static boolean isValidWeight(double weight){
        if(weight < 0 || 500 < weight){
            return false;
        }
        return true;
    }
    
    public String toDataString() {
        return String.format("%s;%d;%.2f",name,age,weight);
    }
    
    @Override
    public String toString(){
        return String.format("%s is %dy/o %.2ftall",name,age,weight);
    }
    
    public static Comparator<Person> sortByName = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2){
            return p1.getName().compareTo(p2.getName());
        }
    };
    
    public static Comparator<Person> sortByAge = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2){
            return p1.getAge() - p2.getAge();
        }
    };
    
    public static Comparator<Person> sortByWeight = new Comparator<Person>(){
        @Override
        public int compare(Person p1, Person p2){
            return Double.compare(p1.getWeight(), p2.getWeight());
        }
    };
}

class DataInvalidException extends RuntimeException {
    public DataInvalidException(String msg){
        super(msg);
    }
}