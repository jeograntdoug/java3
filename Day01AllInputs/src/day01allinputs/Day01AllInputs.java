/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day01allinputs;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author 1898918
 */
public class Day01AllInputs extends javax.swing.JFrame {

    /**
     * Creates new form Day01AllInputs
     */
    public Day01AllInputs() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnGrpAge = new javax.swing.ButtonGroup();
        lblName = new javax.swing.JLabel();
        lblAge = new javax.swing.JLabel();
        lblPets = new javax.swing.JLabel();
        lblConti = new javax.swing.JLabel();
        lblTemp = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        rbAgeBelow18 = new javax.swing.JRadioButton();
        rbAgeBtw18To35 = new javax.swing.JRadioButton();
        rbAgeAbove36 = new javax.swing.JRadioButton();
        cbPetsCat = new javax.swing.JCheckBox();
        cbPetsDog = new javax.swing.JCheckBox();
        cbPetsOther = new javax.swing.JCheckBox();
        comboConti = new javax.swing.JComboBox<>();
        sliderTemp = new javax.swing.JSlider();
        btRegister = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        lblName.setText("Name:");

        lblAge.setText("Age:");

        lblPets.setText("Pets:");

        lblConti.setText("Continent of Residence: ");

        lblTemp.setText("Perfered Temp C: ");

        btnGrpAge.add(rbAgeBelow18);
        rbAgeBelow18.setSelected(true);
        rbAgeBelow18.setText("below 18");

        btnGrpAge.add(rbAgeBtw18To35);
        rbAgeBtw18To35.setText("18-35");

        btnGrpAge.add(rbAgeAbove36);
        rbAgeAbove36.setText("36 and up");

        cbPetsCat.setText("Cat");

        cbPetsDog.setText("Dog");

        cbPetsOther.setText("Other");

        comboConti.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "America", "Europe", "Asia", "Africa", "Australia", "Antarctica" }));

        sliderTemp.setMajorTickSpacing(5);
        sliderTemp.setMaximum(35);
        sliderTemp.setMinimum(15);
        sliderTemp.setMinorTickSpacing(1);
        sliderTemp.setPaintLabels(true);
        sliderTemp.setPaintTicks(true);
        sliderTemp.setSnapToTicks(true);
        sliderTemp.setToolTipText("Temperature in Celsius");
        sliderTemp.setValue(22);

        btRegister.setText("Register Me");
        btRegister.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRegisterActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblTemp)
                                .addGap(20, 20, 20)
                                .addComponent(sliderTemp, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(lblPets, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblAge, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(lblName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(cbPetsCat)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(cbPetsDog)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(cbPetsOther))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(rbAgeBelow18)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rbAgeBtw18To35)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(rbAgeAbove36))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblConti)
                                        .addGap(18, 18, 18)
                                        .addComponent(comboConti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(35, 35, 35))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btRegister)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAge)
                    .addComponent(rbAgeBelow18)
                    .addComponent(rbAgeBtw18To35)
                    .addComponent(rbAgeAbove36))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblPets)
                    .addComponent(cbPetsCat)
                    .addComponent(cbPetsDog)
                    .addComponent(cbPetsOther))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblConti)
                    .addComponent(comboConti, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sliderTemp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblTemp)
                        .addGap(16, 16, 16)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(btRegister)
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    private void btRegisterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRegisterActionPerformed
        String name = tfName.getText();
        if(name.isEmpty()){
            errorMsg("name");
            return;
        }
        String age = getAge();
        if(age.isEmpty()){
            errorMsg("Age");
            return;
        }
        String pets = getPets();
        String conti = comboConti.getSelectedItem().toString();
        
        String tempC = sliderTemp.getValue() + "";
        
        String dataLine = String.join(";",name,age,pets,conti,tempC);
        
        JOptionPane.showMessageDialog(this,
                    dataLine,
                    "is inserted",
                    JOptionPane.INFORMATION_MESSAGE);
        
        saveData(dataLine);
    }//GEN-LAST:event_btRegisterActionPerformed

    static File file = new File("registerList.txt");
    
    private void saveData(String dataStr){
        try(PrintWriter fileOutput = new PrintWriter(file)){
            fileOutput.write(dataStr + "\n");
        } catch(IOException ex){
            System.out.println("Fetal Error: " + ex.getMessage());
        }
    }
    private String getAge(){
        if(rbAgeBelow18.isSelected()){ return "below18"; }
        if(rbAgeBtw18To35.isSelected()){ return "btw18To35"; }
        if(rbAgeAbove36.isSelected()){ return "above36"; }
        return "";
    }
    
    private String getPets(){
        String petStr = "";
        int count = 0;
        if(cbPetsCat.isSelected()){ 
            petStr += count == 0? "cat":",cat";
            count++;
        }
        if(cbPetsDog.isSelected()){
            petStr += count == 0? "dog":",dog";
            count++;
        }
        if(cbPetsOther.isSelected()){
            petStr += count == 0? "other":",other";
            count++;
        }
        return petStr;
    }
    
    private void errorMsg(String str){
        JOptionPane.showMessageDialog(this,
                    str + "is not valid",
                    "input error",
                    JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day01AllInputs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day01AllInputs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day01AllInputs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day01AllInputs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day01AllInputs().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btRegister;
    private javax.swing.ButtonGroup btnGrpAge;
    private javax.swing.JCheckBox cbPetsCat;
    private javax.swing.JCheckBox cbPetsDog;
    private javax.swing.JCheckBox cbPetsOther;
    private javax.swing.JComboBox<String> comboConti;
    private javax.swing.JLabel lblAge;
    private javax.swing.JLabel lblConti;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPets;
    private javax.swing.JLabel lblTemp;
    private javax.swing.JRadioButton rbAgeAbove36;
    private javax.swing.JRadioButton rbAgeBelow18;
    private javax.swing.JRadioButton rbAgeBtw18To35;
    private javax.swing.JSlider sliderTemp;
    private javax.swing.JTextField tfName;
    // End of variables declaration//GEN-END:variables
}
