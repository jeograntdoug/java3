/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz1todo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author 1898918
 */
enum Status { Pending, Done, Delegated }

class Todo {
    private String task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-*?%#@(),./\ only
    private int difficulty; // 1-5, as slider
    private Date dueDate; // year 1900-2100 both inclusive, use formatted field
    private Status status; // enum, combo box
    
    static SimpleDateFormat dateFormatForFile = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Constructor for loading data from file
     * @param dataLine one line data. task,difficulty,duedate, and status ";" seperated
     * @throws InvalidValueException : validation fails
     */
    public Todo(String dataLine)
            throws InvalidValueException
    {
        String[] data = dataLine.split(";");
        if(data.length != 4){
            throw new InvalidValueException("Data line is not right format : " + dataLine);
        }
        setTask(data[0]);
        setDifficulty(data[1]);
        setDueDate(data[2]);
        setStatus(data[3]);
    }
    
    /**
     * Constructor general
     * @param task String
     * @param difficulty integer
     * @param dueDate Date
     * @param status Status(Enum)
     * 
     * @throws InvalidValueException : validation fails
     */
    public Todo(String task,int difficulty,Date dueDate, Status status)
            throws InvalidValueException
    {
        setTask(task);
        setDifficulty(difficulty);
        setDueDate(dueDate);
        setStatus(status);
    }
    
    /**
     * Constructor to task String type directly from input field in the form
     * 
     * @param task
     * @param difficulty
     * @param dueDate
     * @param status
     * 
     * @throws InvalidValueException : validation fails
     */
    public Todo(String task,String difficulty,String dueDate, String status)
            throws InvalidValueException
    {
        setTask(task);
        setDifficulty(difficulty);
        // TODO: try to make general setDueDate(String) for both date format
        setDueDate(dueDate.replace("/", "-"));
        setStatus(status);
    }
    
   
    public String getTask() {
        return task;
    }
    
    public int getDifficulty() {
        return difficulty;
    }

    public Date getDueDate() {
        return dueDate;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public void setTask(String task) 
        throws InvalidValueException
    {
        // 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-*?%#@(),./\ only
        if(!task.matches("[a-zA-Z0-9 _*?%#@(),.\\/\\\\-]{1,100}")){
            throw new InvalidValueException("Task must be 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-*?%#@(),./\\ only. But \"" + task + "\" given");
        }
        this.task = task;
    }

    
    public void setDifficulty(int difficulty)
        throws InvalidValueException
    {
        // 1-5, as slider
        if(difficulty >5 || difficulty<1){
            throw new InvalidValueException("Defficulty must be 1-5 Integer. But \""
            + difficulty + "\" given");
        }
        this.difficulty = difficulty;
    }

    public void setDifficulty(String difficultyStr)
        throws InvalidValueException
    {
        try{
            int diff = Integer.parseInt(difficultyStr);
            setDifficulty(diff);
            
        } catch (NumberFormatException ex){
            throw new InvalidValueException("Defficulty must be 1-5 Integer.But \""
            + difficulty + "\" given");
        }
    }
    
    
    public void setDueDate(Date dueDate)
        throws InvalidValueException
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dueDate);
        int year = cal.get(Calendar.YEAR);
        
        // year 1900-2100 both inclusive
        if(year < 1900 || year > 2100){
            throw new InvalidValueException("Due date must be 1900 ~ 2100. But \""
            + dateFormatForFile.format(dueDate) + "\" given");
        }
        
        this.dueDate = dueDate;
    }
    
    public void setDueDate(String dueDateStr)
        throws InvalidValueException
    {
        try{
            setDueDate(dateFormatForFile.parse(dueDateStr));
            
        }catch (ParseException ex){
            throw new InvalidValueException("DueDate format must be yyyy-MM-dd. But \""
            + dueDateStr + "\" given");
        }
    }

    
    public void setStatus(Status status) 
    {
        this.status = status;
    }
    public void setStatus(String statusStr)
        throws InvalidValueException
    {
        try{
            setStatus(Status.valueOf(statusStr));
        } catch (IllegalArgumentException ex){
            throw new InvalidValueException("Status must be Pending, Done or Delegated. But \""
            + statusStr + "\" given");
        }
    }   
       
    public String toDataString() {
        return String.format("%s;%d;%s;%s",
                task,
                difficulty,
                dateFormatForFile.format(dueDate),
                status
        );
    }
    
    @Override
    public String toString() {
        return String.format("%s by %s /diff.%d,%s",
                task,
                dateFormatForFile.format(dueDate),
                difficulty,
                status
        );
    }
}

class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}