package day06birthdays;

import java.awt.image.WritableRenderedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.jdesktop.swingx.JXDatePicker;

public class Day06Birthdays extends javax.swing.JFrame {

    final String FILE_NAME = "bdays.txt";
    DefaultListModel<Person> modelPeopleList = new DefaultListModel<>();
    DefaultListModel<String> modelBirthdayList = new DefaultListModel<>();
    
    UtilDateModel model;
    JDatePickerImpl datePicker;
    
    Comparator<Person> crtSortBy = Person.compareByName;
    
    long prevLeftClk = 0;

    enum Button {
        Add, Edit
    }
    Button dlgFrom;

    public Day06Birthdays() {
        initComponents();

        loadDataFromFile();
        sortPeopleList();
        addDatePicker();

    }

    /**
     * dlgAddEdit_pnPlaceHolder : Panel(card layout) as a Place holder for
     * datePicker
     */
    void countBirthday(int month,Map<Month,Integer> statsMap){
        int count = 0;
            switch(month)
            {
                case 0:
                    count = statsMap.get(Month.JANUARY);
                    statsMap.put(Month.JANUARY,++count);
                    break;
                case 1:
                    count = statsMap.get(Month.FEBRUARY);
                    statsMap.put(Month.FEBRUARY,++count);
                    break;
                case 2:
                    count = statsMap.get(Month.MARCH);
                    statsMap.put(Month.MARCH,++count);
                    break;
                case 3:
                    count = statsMap.get(Month.APRIL);
                    statsMap.put(Month.APRIL,++count);
                    break;
                case 4:
                    count = statsMap.get(Month.MAY);
                    statsMap.put(Month.MAY,++count);
                    break;
                case 5:
                    count = statsMap.get(Month.JUNE);
                    statsMap.put(Month.JUNE,++count);
                    break;
                case 6:
                    count = statsMap.get(Month.JULY);
                    statsMap.put(Month.JULY,++count);
                    break;
                case 7:
                    count = statsMap.get(Month.AUGUST);
                    statsMap.put(Month.AUGUST,++count);
                    break;
                case 8:
                    count = statsMap.get(Month.SEPTEMBER);
                    statsMap.put(Month.SEPTEMBER,++count);
                    break;
                case 9:
                    count = statsMap.get(Month.OCTOBER);
                    statsMap.put(Month.OCTOBER,++count);
                    break;
                case 10:
                    count = statsMap.get(Month.NOVEMBER);
                    statsMap.put(Month.NOVEMBER,++count);
                    break;
                case 11:
                    count = statsMap.get(Month.DECEMBER);
                    statsMap.put(Month.DECEMBER,++count);
                    break;
                default:
                    System.out.println("INTERNAL ERROR: Convert Month from Number to Enum Month");
                    return; 
            }
    }
    
    void displayStats(){
        Map<Month,Integer> statsMap = new HashMap<>();
        for(Month m : Month.values()){
            statsMap.put(m, 0);
        }
        
        int period = 7;
        int bDayCount = modelPeopleList.size();
        int upcomingBDay = 0;
        for (Object o : modelPeopleList.toArray()) {
            if (((Person) o).getDaysTillBirthDay() <= period) {
                upcomingBDay++;
            }
            
            int month = ((Person)o).getCalBirthDay().get(Calendar.MONTH);
            
            countBirthday(month, statsMap);
        }
        
        dlgStats_lblTotalFriends.setText(bDayCount + "");
        dlgStats_lblBdayIn7days.setText(upcomingBDay + "");
        
        modelBirthdayList.clear();
        int dataMax = 0;
        
        for(Month m : Month.values()){
            
            int value = statsMap.get(m);
            
            if(value > dataMax){ dataMax = value; }
            
            modelBirthdayList.addElement(
                    String.format(
                            "%s: %d bdays",
                            m.toString(),
                            statsMap.get(m)
                    )
            );
        }
        
        statsCanvasPanel1.setDataMax(dataMax);
        statsCanvasPanel1.setStatsMap(statsMap);
    }
    void setDlgAddEditForEdit(){
        Person selPerson = lstPeople.getSelectedValue();
        if(selPerson == null){
            System.out.println("INTERNAL ERROR: Must not happen");
            return;
        }
        
        dlgAddEdit.pack();
        dlgAddEdit.setLocationRelativeTo(this);
        dlgAddEdit.setVisible(true);

        dlgAddEdit_btAddEdit.setText("Edit");
        
        
        dlgAddEdit_tfName.setText(selPerson.getName());
        model.setValue(selPerson.getBirthDay());
        dlgFrom = Button.Edit;
    }
    
    void addDatePicker() {
        model = new UtilDateModel();
        //model.setDate(20,04,2014);
        // Need this...
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        // Don't know about the formatter, but there it is...
        datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());

        dlgAddEdit_pnPlaceHolder.add(datePicker);
    }

    void sortPeopleList() {
        if (modelPeopleList.size() == 0) {
            return;
        }

        ArrayList<Person> tmpList = new ArrayList<Person>();
        for (Object o : modelPeopleList.toArray()) {
            tmpList.add((Person) o);
        }
        tmpList.sort(crtSortBy);

        modelPeopleList.clear();
        modelPeopleList.addAll(tmpList);
    }

    void updateSatus() {
        int period = 7;
        int bDayCount = modelPeopleList.size();
        int upcomingBDay = 0;
        for (Object o : modelPeopleList.toArray()) {
            if (((Person) o).getDaysTillBirthDay() <= period) {
                upcomingBDay++;
            }
        }

        lblStatus.setText(
                String.format(
                        "%d bdays known, %d upcoming in next %d days",
                        bDayCount,
                        upcomingBDay,
                        period
                )
        );
    }

    void loadDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File("bdays.csv"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String dataLine = fileInput.nextLine();
                    modelPeopleList.addElement(new Person(dataLine));
                } catch (DataInvalidException ex) {
                    System.out.println("WARNING:Loading data:" + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("ERROR:Cannot read file");
            JOptionPane.showMessageDialog(this,
                    "Cannot read file:" + FILE_NAME,
                    "File Loading Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }

    void saveDataToFile(String fileName) {
        try ( PrintWriter fileOutput = new PrintWriter(new File(fileName))) {
            for (Object o : modelPeopleList.toArray()) {
                fileOutput.println(((Person) o).toDataString());
            }
        } catch (IOException ex) {
            System.out.println("ERROR:Cannot open file");
            JOptionPane.showMessageDialog(this,
                    "Cannot open file:" + fileName,
                    "File Saving Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        btnGrpSortBy = new javax.swing.ButtonGroup();
        dlgAddEdit = new javax.swing.JDialog();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dlgAddEdit_tfName = new javax.swing.JTextField();
        dlgAddEdit_btAddEdit = new javax.swing.JButton();
        dlgAddEdit_btCancel = new javax.swing.JButton();
        dlgAddEdit_pnPlaceHolder = new javax.swing.JPanel();
        popupOnlstPeople = new javax.swing.JPopupMenu();
        pulstPeople_miDelete = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        pulstPeople_miExit = new javax.swing.JMenuItem();
        dlgStats = new javax.swing.JDialog();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        dlgStats_lblTotalFriends = new javax.swing.JLabel();
        dlgStats_lblBdayIn7days = new javax.swing.JLabel();
        dlgStats_btDismiss = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        dlgStats_lstBirthdayPerMonth = new javax.swing.JList<>();
        statsCanvasPanel1 = new day06birthdays.StatsCanvasPanel();
        lblStatus = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstPeople = new javax.swing.JList<>();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        miExport = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        rdbtSortByName = new javax.swing.JRadioButtonMenuItem();
        rdbrSortByBirthDay = new javax.swing.JRadioButtonMenuItem();
        rdbtSortByDaysTillBirth = new javax.swing.JRadioButtonMenuItem();
        mAdd = new javax.swing.JMenu();
        mStats = new javax.swing.JMenu();

        dlgAddEdit.setAlwaysOnTop(true);
        dlgAddEdit.setResizable(false);

        jLabel1.setText("Name: ");

        jLabel2.setText("BirthDay:");

        dlgAddEdit_btAddEdit.setText("Add");
        dlgAddEdit_btAddEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btAddEditActionPerformed(evt);
            }
        });

        dlgAddEdit_btCancel.setText("Cancel");
        dlgAddEdit_btCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgAddEdit_btCancelActionPerformed(evt);
            }
        });

        dlgAddEdit_pnPlaceHolder.setLayout(new java.awt.CardLayout());

        javax.swing.GroupLayout dlgAddEditLayout = new javax.swing.GroupLayout(dlgAddEdit.getContentPane());
        dlgAddEdit.getContentPane().setLayout(dlgAddEditLayout);
        dlgAddEditLayout.setHorizontalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddEditLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(dlgAddEdit_btCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dlgAddEdit_btAddEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dlgAddEditLayout.createSequentialGroup()
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(dlgAddEditLayout.createSequentialGroup()
                                .addComponent(dlgAddEdit_tfName)
                                .addGap(6, 6, 6))
                            .addComponent(dlgAddEdit_pnPlaceHolder, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        dlgAddEditLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {dlgAddEdit_btAddEdit, dlgAddEdit_btCancel});

        dlgAddEditLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2});

        dlgAddEditLayout.setVerticalGroup(
            dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgAddEditLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(dlgAddEdit_tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(dlgAddEdit_pnPlaceHolder, javax.swing.GroupLayout.DEFAULT_SIZE, 17, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dlgAddEditLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dlgAddEdit_btAddEdit)
                    .addComponent(dlgAddEdit_btCancel))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pulstPeople_miDelete.setText("Delete");
        pulstPeople_miDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulstPeople_miDeleteActionPerformed(evt);
            }
        });
        popupOnlstPeople.add(pulstPeople_miDelete);
        popupOnlstPeople.add(jSeparator2);

        pulstPeople_miExit.setText("Cancel");
        pulstPeople_miExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pulstPeople_miExitActionPerformed(evt);
            }
        });
        popupOnlstPeople.add(pulstPeople_miExit);

        dlgStats.setResizable(false);

        jLabel3.setText("Total No of friends: ");

        jLabel5.setText("BirthDay is coming in 7 days: ");

        jLabel6.setText("BirthDays per month: ");

        dlgStats_lblTotalFriends.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dlgStats_lblTotalFriends.setText("0");

        dlgStats_lblBdayIn7days.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        dlgStats_lblBdayIn7days.setText("0");

        dlgStats_btDismiss.setText("Dissmiss");
        dlgStats_btDismiss.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dlgStats_btDismissActionPerformed(evt);
            }
        });

        dlgStats_lstBirthdayPerMonth.setModel(modelBirthdayList);
        dlgStats_lstBirthdayPerMonth.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(dlgStats_lstBirthdayPerMonth);

        statsCanvasPanel1.setMaximumSize(new java.awt.Dimension(430, 430));
        statsCanvasPanel1.setMinimumSize(new java.awt.Dimension(300, 100));
        statsCanvasPanel1.setPreferredSize(new java.awt.Dimension(300, 100));

        javax.swing.GroupLayout statsCanvasPanel1Layout = new javax.swing.GroupLayout(statsCanvasPanel1);
        statsCanvasPanel1.setLayout(statsCanvasPanel1Layout);
        statsCanvasPanel1Layout.setHorizontalGroup(
            statsCanvasPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        statsCanvasPanel1Layout.setVerticalGroup(
            statsCanvasPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 133, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout dlgStatsLayout = new javax.swing.GroupLayout(dlgStats.getContentPane());
        dlgStats.getContentPane().setLayout(dlgStatsLayout);
        dlgStatsLayout.setHorizontalGroup(
            dlgStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgStatsLayout.createSequentialGroup()
                .addGroup(dlgStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dlgStatsLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(dlgStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(dlgStatsLayout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dlgStats_lblTotalFriends, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(dlgStatsLayout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(dlgStats_lblBdayIn7days, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(statsCanvasPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(dlgStatsLayout.createSequentialGroup()
                        .addGap(113, 113, 113)
                        .addComponent(dlgStats_btDismiss)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        dlgStatsLayout.setVerticalGroup(
            dlgStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dlgStatsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dlgStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(dlgStats_lblTotalFriends, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(dlgStatsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(dlgStats_lblBdayIn7days))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statsCanvasPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(dlgStats_btDismiss)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblStatus.setText("...");
        getContentPane().add(lblStatus, java.awt.BorderLayout.PAGE_END);

        lstPeople.setModel(modelPeopleList);
        lstPeople.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstPeople.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                lstPeopleFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                lstPeopleFocusLost(evt);
            }
        });
        lstPeople.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstPeopleMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lstPeopleMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstPeopleMouseReleased(evt);
            }
        });
        lstPeople.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstPeopleValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstPeople);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jMenu1.setText("File");

        miExport.setText("Export to CSV...");
        miExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExportActionPerformed(evt);
            }
        });
        jMenu1.add(miExport);
        jMenu1.add(jSeparator1);

        miExit.setText("Exit");
        miExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                miExitActionPerformed(evt);
            }
        });
        jMenu1.add(miExit);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("SortBy");

        btnGrpSortBy.add(rdbtSortByName);
        rdbtSortByName.setSelected(true);
        rdbtSortByName.setText("Name");
        rdbtSortByName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbtSortByNameActionPerformed(evt);
            }
        });
        jMenu2.add(rdbtSortByName);

        btnGrpSortBy.add(rdbrSortByBirthDay);
        rdbrSortByBirthDay.setText("BirthDay");
        rdbrSortByBirthDay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbrSortByBirthDayActionPerformed(evt);
            }
        });
        jMenu2.add(rdbrSortByBirthDay);

        btnGrpSortBy.add(rdbtSortByDaysTillBirth);
        rdbtSortByDaysTillBirth.setText("Days Till BirthDay");
        rdbtSortByDaysTillBirth.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdbtSortByDaysTillBirthActionPerformed(evt);
            }
        });
        jMenu2.add(rdbtSortByDaysTillBirth);

        jMenuBar1.add(jMenu2);

        mAdd.setText("Add");
        mAdd.addMenuKeyListener(new javax.swing.event.MenuKeyListener() {
            public void menuKeyPressed(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyReleased(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyTyped(javax.swing.event.MenuKeyEvent evt) {
                mAddMenuKeyTyped(evt);
            }
        });
        mAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mAddMouseClicked(evt);
            }
        });
        jMenuBar1.add(mAdd);

        mStats.setText("Statistics");
        mStats.addMenuKeyListener(new javax.swing.event.MenuKeyListener() {
            public void menuKeyPressed(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyReleased(javax.swing.event.MenuKeyEvent evt) {
            }
            public void menuKeyTyped(javax.swing.event.MenuKeyEvent evt) {
                mStatsMenuKeyTyped(evt);
            }
        });
        mStats.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mStatsMouseClicked(evt);
            }
        });
        jMenuBar1.add(mStats);

        setJMenuBar(jMenuBar1);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void miExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExitActionPerformed
        saveDataToFile(FILE_NAME);
        dispose();
    }//GEN-LAST:event_miExitActionPerformed

    private void miExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_miExportActionPerformed
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            File targetFile = fileChooser.getSelectedFile();
            String filePath = targetFile.getPath();
            if (!filePath.matches(".*.csv")) {
                filePath += ".csv";
            }

            saveDataToFile(filePath);
            lblStatus.setText("Saved: \"" + filePath + "\"");
        }
    }//GEN-LAST:event_miExportActionPerformed

    private void lstPeopleValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstPeopleValueChanged
//        updateSatus();
    }//GEN-LAST:event_lstPeopleValueChanged

    private void mAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mAddMouseClicked
        dlgAddEdit.pack();
        dlgAddEdit.setLocationRelativeTo(this);
        dlgAddEdit.setVisible(true);
        dlgFrom = Button.Add;
        dlgAddEdit_btAddEdit.setText("Add");
    }//GEN-LAST:event_mAddMouseClicked

    private void mAddMenuKeyTyped(javax.swing.event.MenuKeyEvent evt) {//GEN-FIRST:event_mAddMenuKeyTyped
        dlgAddEdit.pack();
        dlgAddEdit.setLocationRelativeTo(this);
        dlgAddEdit.setVisible(true);
        dlgFrom = Button.Add;
        dlgAddEdit_btAddEdit.setText("Add");
    }//GEN-LAST:event_mAddMenuKeyTyped

    private void dlgAddEdit_btCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btCancelActionPerformed
        dlgAddEdit.dispose();
    }//GEN-LAST:event_dlgAddEdit_btCancelActionPerformed

    private void dlgAddEdit_btAddEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgAddEdit_btAddEditActionPerformed

        try {
            Person newPerson = new Person(
                    dlgAddEdit_tfName.getText(),
                    (Date) datePicker.getModel().getValue()
            );
            
            if (null == dlgFrom) {
                System.out.println("INTERNAL ERROR: Must not happened");
                dlgAddEdit.dispose();
            }
            
            String statusMsg = "";
            switch (dlgFrom) 
            {
                case Add:
                    modelPeopleList.addElement(newPerson);
                    statusMsg = "Adding Success: " + newPerson.toString();
                    break;
                case Edit:
                    int index = lstPeople.getSelectedIndex();
                    if(index < 0){
                        System.out.println("INTERNAL ERROR: Must not happened");
                        dlgAddEdit.dispose();
                    }   
                    modelPeopleList.remove(index);
                    modelPeopleList.add(index, newPerson);
                    statusMsg = "Editing Success: " + newPerson.toString();
                    break;
                default:
                    System.out.println("INTERNAL ERROR: Must not happened");
                    dlgAddEdit.dispose();
                    break;
            }
            
            lblStatus.setText(statusMsg);
            sortPeopleList();
        } catch (DataInvalidException ex) {
            lblStatus.setText("Fail adding: Invalid Input...");
            JOptionPane.showMessageDialog(this,
                    "Invalid Input: " + ex.getMessage(),
                    "Adding Error",
                    JOptionPane.ERROR_MESSAGE
            );
            return;
        }

        // close dlgAddEdit thread
        dlgAddEdit.dispose();
    }//GEN-LAST:event_dlgAddEdit_btAddEditActionPerformed

    private void rdbtSortByNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbtSortByNameActionPerformed
        crtSortBy = Person.compareByName;
        sortPeopleList();
    }//GEN-LAST:event_rdbtSortByNameActionPerformed

    private void rdbrSortByBirthDayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbrSortByBirthDayActionPerformed
        crtSortBy = Person.compareByBirthDay;
        sortPeopleList();
    }//GEN-LAST:event_rdbrSortByBirthDayActionPerformed

    private void rdbtSortByDaysTillBirthActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdbtSortByDaysTillBirthActionPerformed
        crtSortBy = Person.compareByDDay;
        sortPeopleList();
    }//GEN-LAST:event_rdbtSortByDaysTillBirthActionPerformed

    private void lstPeopleFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_lstPeopleFocusLost
        popupOnlstPeople.setVisible(false);
    }//GEN-LAST:event_lstPeopleFocusLost

    private void lstPeopleMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPeopleMousePressed
        updateSatus();
        if(SwingUtilities.isRightMouseButton(evt)){
            lstPeople.setSelectedIndex(lstPeople.locationToIndex(evt.getPoint()));
            popupOnlstPeople.setLocation(evt.getXOnScreen(), evt.getYOnScreen());
            popupOnlstPeople.setVisible(true);
        } else {
            popupOnlstPeople.setVisible(false);
        }
    }//GEN-LAST:event_lstPeopleMousePressed

    private void lstPeopleMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPeopleMouseReleased
        updateSatus();
        if(SwingUtilities.isRightMouseButton(evt)){
            lstPeople.setSelectedIndex(lstPeople.locationToIndex(evt.getPoint()));
            popupOnlstPeople.setLocation(evt.getXOnScreen(), evt.getYOnScreen());
            popupOnlstPeople.setVisible(true);
        } else {
            popupOnlstPeople.setVisible(false);
            
            long crtLeftClk = System.currentTimeMillis();
            
            if(crtLeftClk - prevLeftClk < 100){
                setDlgAddEditForEdit();
            }
            prevLeftClk = crtLeftClk;
        }
        
    }//GEN-LAST:event_lstPeopleMouseReleased

    private void pulstPeople_miExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulstPeople_miExitActionPerformed
        popupOnlstPeople.setVisible(false);
    }//GEN-LAST:event_pulstPeople_miExitActionPerformed

    private void pulstPeople_miDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pulstPeople_miDeleteActionPerformed
        int index = lstPeople.getSelectedIndex();
        if(index < 0){
            return;
        }
        modelPeopleList.remove(index);
        popupOnlstPeople.setVisible(false);
    }//GEN-LAST:event_pulstPeople_miDeleteActionPerformed

    private void lstPeopleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPeopleMouseClicked
        
    }//GEN-LAST:event_lstPeopleMouseClicked

    private void lstPeopleFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_lstPeopleFocusGained
        
    }//GEN-LAST:event_lstPeopleFocusGained

    private void mStatsMenuKeyTyped(javax.swing.event.MenuKeyEvent evt) {//GEN-FIRST:event_mStatsMenuKeyTyped
        dlgStats.pack();
        dlgStats.setLocationRelativeTo(this);
        dlgStats.setVisible(true);
        displayStats();
       
    }//GEN-LAST:event_mStatsMenuKeyTyped

    private void mStatsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mStatsMouseClicked
        dlgStats.pack();
        dlgStats.setLocationRelativeTo(this);
        dlgStats.setVisible(true);
        displayStats();
    }//GEN-LAST:event_mStatsMouseClicked

    private void dlgStats_btDismissActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dlgStats_btDismissActionPerformed
        dlgStats.dispose();
    }//GEN-LAST:event_dlgStats_btDismissActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day06Birthdays.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day06Birthdays.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day06Birthdays.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day06Birthdays.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day06Birthdays().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup btnGrpSortBy;
    private javax.swing.JDialog dlgAddEdit;
    private javax.swing.JButton dlgAddEdit_btAddEdit;
    private javax.swing.JButton dlgAddEdit_btCancel;
    private javax.swing.JPanel dlgAddEdit_pnPlaceHolder;
    private javax.swing.JTextField dlgAddEdit_tfName;
    private javax.swing.JDialog dlgStats;
    private javax.swing.JButton dlgStats_btDismiss;
    private javax.swing.JLabel dlgStats_lblBdayIn7days;
    private javax.swing.JLabel dlgStats_lblTotalFriends;
    private javax.swing.JList<String> dlgStats_lstBirthdayPerMonth;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JList<Person> lstPeople;
    private javax.swing.JMenu mAdd;
    private javax.swing.JMenu mStats;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenuItem miExport;
    private javax.swing.JPopupMenu popupOnlstPeople;
    private javax.swing.JMenuItem pulstPeople_miDelete;
    private javax.swing.JMenuItem pulstPeople_miExit;
    private javax.swing.JRadioButtonMenuItem rdbrSortByBirthDay;
    private javax.swing.JRadioButtonMenuItem rdbtSortByDaysTillBirth;
    private javax.swing.JRadioButtonMenuItem rdbtSortByName;
    private day06birthdays.StatsCanvasPanel statsCanvasPanel1;
    // End of variables declaration//GEN-END:variables
}
