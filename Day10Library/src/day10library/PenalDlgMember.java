/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10library;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author heokc
 */
public class PenalDlgMember extends javax.swing.JPanel {
    private final Pattern imgExtPtn = Pattern.compile("\\.(jpg|jpeg|png|gif)$");
    // set file filter for *.jpg, *.jpeg, *.gif, *.png extentions 
    private final FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "Image file(*.jpg, *.jpeg, *.gif, *.png)"
                        , "jpg","jpeg","gif","png"
                );
    
    private final String DEFAULT_PHOTO_MESSAGE = "150x150 pixels, clickable";
    
    ArrayList<Member> memberList = new ArrayList<>();
    DefaultListModel<Member> modelMemberList = new DefaultListModel<>();
    
    private BufferedImage bufImage;
    private Database db;
    
    public PenalDlgMember() {
        initComponents();
        
        try{
            db = DatabaseManager.getDatabaseInstance();

        } catch(SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Cannot connect Dababase(ManageMember). Program exit",
                    "DABABASE ERROR",
                    JOptionPane.ERROR_MESSAGE
            );
        }

        comboMaxBooks.setRenderer(new ExtendedListCellRenderer());
        
        fileChooser.setFileFilter(filter);
    }

     public void reloadMemberList(){ 
        try{
            tfSearch.setText("");// clear search text field
            memberList = db.getMemberList();
            modelMemberList.clear();
            modelMemberList.addAll(memberList);
        } catch (SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Cannot read data",
                    "ERROR Data load",
                    JOptionPane.ERROR_MESSAGE
            );
            System.exit(1);
        }
    }
     
     
    // reset Input fields by select or not
    private void resetInputFields(){
        Member member = lstMember.getSelectedValue();
        if(member == null){
            lblIdVal.setText("-");
            tfName.setText("");
            tfEmail.setText("");
            comboMaxBooks.setSelectedIndex(-1);
            lblPhotoVal.setIcon(null);
            lblPhotoVal.setText(DEFAULT_PHOTO_MESSAGE);
            
           
            btDelete.setEnabled(false);
            btUpdate.setEnabled(false);
            return;
        } 
        
        lblIdVal.setText(member.getId() + "");
        tfName.setText(member.getName());
        tfEmail.setText(member.getEmail());
        
        int index = Arrays.binarySearch(Member.MAXBOOK_VALUES, member.getMaxBookAllowed());
        comboMaxBooks.setSelectedIndex(index);
        
        bufImage = member.getPhotoBufImg();
        if(bufImage != null){
            lblPhotoVal.setIcon(new ImageIcon(bufImage));
        }
        
        btDelete.setEnabled(true);
        btUpdate.setEnabled(true);
        
       
    }
    
    // convert image size to 150x150, return converted Image
    public static BufferedImage convertImgSize(BufferedImage originalImg){
        BufferedImage newImg = new BufferedImage(150,150,originalImg.getType());
        Graphics2D g = newImg.createGraphics();
        g.drawImage(originalImg.getScaledInstance(150, 150, Image.SCALE_SMOOTH),null,null);
        g.dispose();
        
        return newImg;
    }
    // Check if file extention is in *.jpg, *.jpeg, *.gif, *.png
    private boolean isValidImgFile(File file){
        Matcher m = imgExtPtn.matcher(file.getName());
        if(!m.find()){
            JOptionPane.showMessageDialog(this,
                    "Not supported Image file chosen:" +file.getName(),
                    "No Image chosen",
                    JOptionPane.ERROR_MESSAGE
            );
            return false;
        }
        return true;
    }
    // Choose Image with file chooser(filter :*.jpg, *.jpeg, *.gif, *.png)
    private File chooseImageFile(){
        if(fileChooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION){
            return null;
        }
        
        File chosenImgFile = fileChooser.getSelectedFile();
        if(chosenImgFile == null){
            JOptionPane.showMessageDialog(this,
                    "Please choose image that you want to add",
                    "No Image is chosen",
                    JOptionPane.ERROR_MESSAGE
            );
            return null;
        }
        return chosenImgFile;
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        fileChooser = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        tfSearch = new javax.swing.JTextField();
        btClear = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstMember = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblIdVal = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        tfEmail = new javax.swing.JTextField();
        comboMaxBooks = new javax.swing.JComboBox<>();
        lblPhotoVal = new javax.swing.JLabel();
        btAdd = new javax.swing.JButton();
        btUpdate = new javax.swing.JButton();
        btDelete = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(647, 344));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Search:");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 14, 43, -1));

        tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfSearchKeyReleased(evt);
            }
        });
        add(tfSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(57, 11, 127, -1));

        btClear.setText("Clear");
        btClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClearActionPerformed(evt);
            }
        });
        add(btClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 10, -1, -1));

        lstMember.setModel(modelMemberList);
        lstMember.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstMember.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstMemberValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstMember);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 37, 372, 297));

        jLabel2.setText("ID:");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 37, 43, -1));

        jLabel3.setText("Name: ");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 63, -1, -1));

        jLabel4.setText("Email:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 92, -1, -1));

        jLabel5.setText("Max Books:");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 121, -1, -1));

        jLabel6.setText("Photo: ");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 147, -1, -1));

        lblIdVal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblIdVal.setText("-");
        add(lblIdVal, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 37, 151, -1));
        add(tfName, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 60, 151, -1));
        add(tfEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(461, 89, 151, -1));

        comboMaxBooks.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "3", "5", "10", "15", "20" }));
        comboMaxBooks.setSelectedIndex(-1);
        add(comboMaxBooks, new org.netbeans.lib.awtextra.AbsoluteConstraints(462, 118, 150, -1));

        lblPhotoVal.setText("150x150 Pixels, Clickable");
        lblPhotoVal.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        lblPhotoVal.setPreferredSize(new java.awt.Dimension(150, 150));
        lblPhotoVal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPhotoValMouseClicked(evt);
            }
        });
        add(lblPhotoVal, new org.netbeans.lib.awtextra.AbsoluteConstraints(462, 147, -1, -1));

        btAdd.setText("Add");
        btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddActionPerformed(evt);
            }
        });
        add(btAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(407, 313, 63, -1));

        btUpdate.setText("Update");
        btUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUpdateActionPerformed(evt);
            }
        });
        add(btUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 313, -1, -1));

        btDelete.setText("Delete");
        btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDeleteActionPerformed(evt);
            }
        });
        add(btDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(551, 313, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void lblPhotoValMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPhotoValMouseClicked
        // Choose Image with file chooser
        File chosenImgFile = chooseImageFile();
        if(chosenImgFile == null){ return; }
        
        // Check if file extention is in *.jpg, *.jpeg, *.gif, *.png
        if(!isValidImgFile(chosenImgFile)){ return; }
        
        // Read image file and convert it to 150x150 size
        //( getScaledInstance() method in Image Class with Image.SCALE_SMOOTH ) 
        try{
            BufferedImage originalImg = ImageIO.read(chosenImgFile);
            
            //convert it to 150x150 size with
            BufferedImage convertedImg = convertImgSize(originalImg);
            lblPhotoVal.setIcon(new ImageIcon(convertedImg));
            bufImage = convertedImg; // For saving/ editing member's info
        } catch (IOException ex){
            JOptionPane.showMessageDialog(this,
                    "Cannot open Image file.",
                    "Reading File Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_lblPhotoValMouseClicked

    private void tfSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSearchKeyReleased
        String searchStr = tfSearch.getText();
        if(searchStr == null) { return; }
        if(searchStr.contains(" ")){ return; }
        
        modelMemberList.clear();
        for(Member m : memberList){
            if(m.isMatches(searchStr)){
                modelMemberList.addElement(m);
            }
        }
    }//GEN-LAST:event_tfSearchKeyReleased

    private void btClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btClearActionPerformed
        tfSearch.setText("");
        modelMemberList.clear();
        modelMemberList.addAll(memberList);
    }//GEN-LAST:event_btClearActionPerformed

    private void lstMemberValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstMemberValueChanged
        resetInputFields();
    }//GEN-LAST:event_lstMemberValueChanged

    private void btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddActionPerformed
        try{
            String name = tfName.getText();
            String email = tfEmail.getText();

            int maxbooks = Member.MAXBOOK_VALUES[comboMaxBooks.getSelectedIndex()];
            Member member = new Member(0,name,email,bufImage,maxbooks,0);
            db.addMember(member);
            reloadMemberList();
            resetInputFields();
        } catch (InvalidMemberDataException | SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Invalid Data:"+ex.getMessage(),
                    "Data Validation Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btAddActionPerformed

    private void btUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUpdateActionPerformed
        
        Member selMember = lstMember.getSelectedValue();
        if(selMember == null){
            JOptionPane.showMessageDialog(this,
                    "Please select one of member",
                    "Updating Member Error",
                    JOptionPane.ERROR_MESSAGE
            );
            return;
        }
        
        try{
            selMember.setName(tfName.getText());
            selMember.setEmail(tfEmail.getText());
            selMember.setMaxBookAllowed(Member.MAXBOOK_VALUES[comboMaxBooks.getSelectedIndex()]);
            selMember.setPhotoBufImg(bufImage);
            
            db.updateMember(selMember);
            reloadMemberList();
            resetInputFields();
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(this,
                    "Database connection fail",
                    "Updating Member Error",
                    JOptionPane.ERROR_MESSAGE
            );
        } catch (InvalidMemberDataException ex){
            JOptionPane.showMessageDialog(this,
                    "Invalid Input:" + ex.getMessage(),
                    "Update Member Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btUpdateActionPerformed

    private void btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDeleteActionPerformed
        try {
            Member selMember = lstMember.getSelectedValue();
            if(selMember == null){ return; }
            
            db.deleteMember(selMember.getId());
            resetInputFields();
            reloadMemberList();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this,
                    "Database connection fail",
                    "Delete Member Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAdd;
    private javax.swing.JButton btClear;
    private javax.swing.JButton btDelete;
    private javax.swing.JButton btUpdate;
    private javax.swing.JComboBox<String> comboMaxBooks;
    private javax.swing.JFileChooser fileChooser;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIdVal;
    private javax.swing.JLabel lblPhotoVal;
    private javax.swing.JList<Member> lstMember;
    private javax.swing.JTextField tfEmail;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfSearch;
    // End of variables declaration//GEN-END:variables
}
