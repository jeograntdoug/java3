/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day10flightsdb;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;



class InvalidDataException extends RuntimeException {
    public InvalidDataException(String msg){
        super(msg);
    }
}

public class Flight {
    private int id;
    private LocalDate onDay;
    private String fromCode;
    private String toCode;
    private Type type;
    private int passengers;
    
    static enum Type { Domestic,International,Private }
    
    static DateTimeFormatter dateFormatDisplay = DateTimeFormatter.ofPattern("d/M/yyyy");
    
    public Flight(int id,LocalDate onDay,String fromCode,String toCode,Type type,int passengers){
        setId(id);
        setOnDay(onDay);
        setFromCode(fromCode);
        setToCode(toCode);
        setType(type);
        setPassengers(passengers);
    }
    public Flight(int id,String onDay,String fromCode,String toCode,String type,int passengers){
        setId(id);
        setOnDay(onDay);
        setFromCode(fromCode);
        setToCode(toCode);
        setType(type);
        setPassengers(passengers);
    }
    public int getId(){ return id; }
    public LocalDate getOnDay(){ return onDay; }
    public String getFromCode() { return fromCode; }
    public String getToCode() { return toCode; }
    public Type getType() { return type; }
    public int getPassengers() { return passengers; }
    
    public void setId(int id){ this.id = id; }
    public void setOnDay(LocalDate onDay){ this.onDay = onDay; }
    public void setOnDay(String onDay) {//String date format:  yyyy-MM-dd
        try{
        setOnDay(LocalDate.parse(onDay)); //ex
        } catch (DateTimeParseException ex){
            throw new InvalidDataException("Invalid Date");
        }
    }//ex
    public void setFromCode(String fromCode){
        if(!isValidCode(fromCode)){ 
            throw new InvalidDataException("Invalid Code");
        }
        this.fromCode = fromCode; 
        
    }
    public void setToCode(String toCode){
        if(!isValidCode(toCode)){ 
            throw new InvalidDataException("Invalid Code");
        }
        this.toCode = toCode; 
        
    }
    public void setType( Type type){ this.type = type;}
    public void setType(String type) {
        try{
            if(type == null){
                throw new InvalidDataException("Type is null");
            }
        
            setType(Type.valueOf(type));
        } catch(IllegalArgumentException ex){
            throw new InvalidDataException("Invalid Type");
        }
    }
    public void setPassengers (int passengers){
        if(passengers > 200 | passengers < 0){
            throw new InvalidDataException("Invalid passengers:" + passengers);
        }
        this.passengers = passengers;
    }
    
    private boolean isValidCode(String code){
        if(code == null){ return false; }
        if(!code.matches("[A-Z]{3,5}")){ return false; }
        
        return true;
    }
    
    @Override
    public String toString(){
        return String.format("Flight:%d, on %s from %s to %s,passengers:%d type:%s",
                id,onDay,fromCode,toCode,passengers,type
        );
    }
    
    public String toDataString(){
        return String.format("%d;%s;%s;%s;%s;%d",id,onDay,fromCode,toCode,type,passengers);
    }
}
