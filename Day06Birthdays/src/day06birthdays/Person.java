
package day06birthdays;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

public class Person {
    private String name;
    private Date birthDay;
    private Calendar calBirthDay;
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    Person (String name, String birthDay){
        setName(name);
        setBirthDay(birthDay);
    }
    
    Person (String name, Date birthDay){
        setName(name);
        setBirthDay(birthDay);
    }
    
    Person (String dataLine){
        String[] data = dataLine.split(",");
        if(data.length != 2){
            throw new DataInvalidException("Wrong data format:" + dataLine);
        }
        setName(data[0]);
        setBirthDay(data[1]);
    }
    
    public String getName() { return name; }
    public Date getBirthDay() { return birthDay; }
    public int getDaysTillBirthDay(){
        Calendar.Builder calBuilder = new Calendar.Builder();
        calBuilder.setInstant(new Date());
        Calendar calNow = calBuilder.build();
        
        Calendar calBirth = (Calendar)calBirthDay.clone();
        calBirth.set(Calendar.YEAR,calNow.get(Calendar.YEAR));
        
        int todayOfYear = calNow.get(Calendar.DAY_OF_YEAR);
        int birthDayOfYear = calBirth.get(Calendar.DAY_OF_YEAR);
        
        int dayDiff = birthDayOfYear - todayOfYear;
        if(dayDiff < 0){
            dayDiff = calNow.getMaximum(Calendar.DAY_OF_YEAR) + dayDiff;
        }
        return dayDiff;
    }
    public Calendar getCalBirthDay() { return calBirthDay; }
    
    private void setName(String name){
        if(!name.matches("[^;]{1,50}")){
            throw new DataInvalidException("Name must be 1-50 chars, no semicolon:"
                + name + " given");
        }
        this.name = name;
    }
    private void setBirthDay(Date birthDay){
        Calendar.Builder calBuilder = new Calendar.Builder();
        calBuilder.setInstant(birthDay);
        calBirthDay = calBuilder.build();
        
        int year = calBirthDay.get(Calendar.YEAR);
        
        //FIXME :  Change maximum year to today
        if(year < 1900 || year > 2020){
            throw new DataInvalidException("Date must be 1900~2100: "
                + dateFormat.format(birthDay) + " given");
        }
        this.birthDay = birthDay;
    }
    private void setBirthDay(String birthDay){
        try {
            setBirthDay(dateFormat.parse(birthDay));
        
         } catch (ParseException ex){
            throw new DataInvalidException("Wrong Date format: " 
                + birthDay + " given");
        }
    }
    
    public String toDataString(){
        return String.format("%s;%s",name,dateFormat.format(birthDay));
    }
    
    @Override
    public String toString(){
        return String.format(
                "%s born %s bday is %s",
                name,
                dateFormat.format(birthDay),
                getDaysTillBirthDay() != 0 ? 
                        "in " + getDaysTillBirthDay() + " Days"  : "Today!"
        );
    }
    public static Comparator<Person> compareByName = new Comparator<>() {
       @Override
       public int compare(Person p1, Person p2){
           return p1.getName().compareTo(p2.getName());
       }
    };
    
    public static Comparator<Person> compareByBirthDay = new Comparator<>() {
        @Override
        public int compare(Person p1, Person p2){
            return p1.getBirthDay().compareTo(p2.getBirthDay());
        }
    };
    
    public static Comparator<Person> compareByDDay = new Comparator<>() {
        @Override
        public int compare(Person p1,Person p2){
            return p1.getDaysTillBirthDay() - p2.getDaysTillBirthDay();
        }
    };
}

class DataInvalidException extends RuntimeException {
    DataInvalidException (String msg){
        super(msg);
    }
}