/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day05cars;

import java.util.Comparator;

/**
 *
 * @author heokc
 */
enum FuelType { Gasoline, Desiel, Electric }
public class Car {
    
    private static int totalCar = 0;
    
    int ID;
    String makeModel;
    double engineSize;
    FuelType fuelType;
    
    Car(String dataLine){
        String data[] = dataLine.split(",");
        if(data.length != 3){
            throw new DataInvalidException("Data line format is not valid");
        }
        setMakeModel(data[0]);
        setEngineSize(data[1]);
        setFuelType(data[2]);
        this.ID = ++totalCar;
    }
    Car(String makeModel, double engineSize, FuelType fuelType){
        setMakeModel(makeModel);
        setEngineSize(engineSize);
        setFuelType(fuelType);
        this.ID = ++totalCar;
    }
    Car(String makeModel, String engineSize, String fuelType){
        setMakeModel(makeModel);
        setEngineSize(engineSize);
        setFuelType(fuelType);
        this.ID = ++totalCar;
    }
    Car(int ID,String makeModel, String engineSize, String fuelType){
        this(makeModel, engineSize, fuelType);
        setID(ID);
    }
    
    public int getID(){ return ID; }
    public String getMakeModel() { return makeModel; }
    public double getEngineSize() { return engineSize; }
    public FuelType getFuelType() { return fuelType; }
    
    public void setID(int ID){
        if(ID <0){
            throw new DataInvalidException("ID Must be greater than 0");
        }
        this.ID = ID;
    }
    public void setMakeModel(String makeModel){
        if(makeModel.contains(",") || makeModel.isEmpty()){
            throw new DataInvalidException("Make Model cannot contain ','");
        }
        this.makeModel = makeModel;
    }
    
    public void setEngineSize(double engineSize){
        if(engineSize <= 0 || 2 <= engineSize ){
            // Must not happend (slider input)
            throw new DataInvalidException("Engine size must be 1 ~ 20");
        }
        this.engineSize = engineSize;
    }
    public void setEngineSize(String engineSize){
        if(engineSize.isEmpty()){
            throw new DataInvalidException("Engine Size cannot be empty");
        }
        
        try{
            setEngineSize(Double.parseDouble(engineSize));
        } catch (NumberFormatException ex){
            throw new DataInvalidException("Engine size must be double");
        }
    }
    
    public void setFuelType(FuelType fuelType){
        this.fuelType = fuelType;
    }
    public void setFuelType(String fuelTypeStr){
        try{
            setFuelType(FuelType.valueOf(fuelTypeStr));
        } catch (IllegalArgumentException ex){
            throw new DataInvalidException("Fuel Type must be Gasoline, Desiel or Electric");
        }
    }
    
    public String toCsvString(){
        return String.format("%s,%.1f,%s",makeModel,engineSize,fuelType);
    }
    
    @Override
    public String toString(){
        return String.format("%d: %s %.1fL %s",ID,makeModel,engineSize,fuelType);
    }
    
    public static Comparator<Car> sortByID = new Comparator<Car>() {
        @Override
        public int compare(Car c1, Car c2){
            return c1.getID() - c2.getID();
        }
    };
    
    public static Comparator<Car> sortByMakeModel = new Comparator<Car>() {
        @Override
        public int compare(Car c1, Car c2){
            return c1.getMakeModel().compareTo(c2.getMakeModel());
        }
    };

    public static Comparator<Car> sortByEngineSize = new Comparator<Car>() {
        @Override
        public int compare(Car c1, Car c2){
            return Double.compare(c1.getEngineSize(), c2.getEngineSize());
        }
    };
    
    public static Comparator<Car> sortByFuelType = new Comparator<Car>() {
        @Override
        public int compare(Car c1, Car c2){
            return c1.getFuelType().toString().compareTo(c2.getFuelType().toString());
        }
    };
}
    

class DataInvalidException extends RuntimeException {
    public DataInvalidException (String msg){
        super(msg);
    }
}