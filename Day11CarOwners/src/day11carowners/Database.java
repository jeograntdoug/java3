/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.DefaultListModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Blob;
import javax.imageio.ImageIO;

class UniqueValueRequiredException extends Exception {
    public UniqueValueRequiredException(String msg) {
        super(msg);
    }
}

public class Database {
    public Connection dbConn;
    
    private final String CONN_STR = "jdbc:mysql://localhost/ipd20carowners?user=root&password=root";
    
    public Database() throws SQLException{
        dbConn = DriverManager.getConnection(CONN_STR);
    }

    ArrayList<Owner> getAllOwners() throws SQLException {
        try{
            PreparedStatement preStmt = dbConn.prepareStatement(
                    "SELECT o.id,o.name,o.photo,COUNT(c.id) as 'carsOwned' "
                    + "FROM owners as o "
                    + "LEFT JOIN cars as c ON o.id = c.ownerId "
                    + "GROUP BY o.id;"
            );
            ResultSet resultSet = preStmt.executeQuery();

            ArrayList<Owner> ownerList = new ArrayList<>();
            while(resultSet.next()){
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                Blob photo = resultSet.getBlob("photo");
                int carsOwned = resultSet.getInt("carsOwned");
                ownerList.add(new Owner(id,name,photo,carsOwned));//ex
            }
            return ownerList;
            
        } catch (InvalidDataException ex){
            throw new SQLException("Invalid data from database");
        }
    }
    
    void addOwner(Owner owner) throws SQLException, UniqueValueRequiredException {
        try {
            PreparedStatement preStmtCheck = dbConn.prepareStatement("SELECT id FROM owners WHERE name=?");
            preStmtCheck.setString(1,owner.getName());
            ResultSet resultSet = preStmtCheck.executeQuery();
            if(resultSet.next()){//If there is same name in the database
                throw new UniqueValueRequiredException(owner.getName() + " is already registered");
            }
            
            
            // If there is no same name in the database
            PreparedStatement preStmtAdd = dbConn.prepareStatement("INSERT INTO owners VALUES (NULL,?,?)");
            preStmtAdd.setString(1,owner.getName());
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] photoByte = null;
            if(owner.getPhoto() != null){
                ImageIO.write(owner.getPhoto(), "png", baos);
                photoByte = baos.toByteArray();
            }
            
            preStmtAdd.setBytes(2,photoByte);
            
            preStmtAdd.executeUpdate();
        } catch (IOException ex) {
            throw new SQLException("INTERNAL ERROR: Photo value is not valid");
        }
    }
    
  
    void updateOwner(Owner owner) throws SQLException, UniqueValueRequiredException {
        try {
            PreparedStatement preStmtCheck = dbConn.prepareStatement("SELECT id FROM owners WHERE name=?");
            preStmtCheck.setString(1,owner.getName());
            ResultSet resultSet = preStmtCheck.executeQuery();
            if(resultSet.next()){//If there is same name in the database && It is not the owner
                if(resultSet.getInt("id") != owner.getId()){
                    throw new UniqueValueRequiredException(owner.getName() + " is already registered");
                }
            }
            
            // If there is no same name in the database
            PreparedStatement preStmtUpdate = dbConn.prepareStatement("UPDATE owners SET name=?,photo=? WHERE id=?");
            preStmtUpdate.setString(1,owner.getName());
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] photoByte = null;
            if(owner.getPhoto() != null){
                ImageIO.write(owner.getPhoto(), "png", baos);
                photoByte = baos.toByteArray();
            }
            
            preStmtUpdate.setBytes(2,photoByte);
            preStmtUpdate.setInt(3, owner.getId());
            preStmtUpdate.executeUpdate();
        } catch (IOException ex) {
            throw new SQLException("INTERNAL ERROR: Photo value is not valid");
        }
    }
    
    void deleteOwner(int id) throws SQLException {
        // consider database settings, one of:
        // RESTRICT | CASCADE | SET NULL (preferred in this case) | NO ACTION | SET DEFAULT
        PreparedStatement preStmtDel = dbConn.prepareStatement("DELETE FROM owners WHERE id=?");
        preStmtDel.setInt(1,id);
        preStmtDel.executeUpdate();
        
    }

    ArrayList<Car> getAllCarsOwnedBy(int ownerId) throws SQLException {
        // return cars owned by a specific owner
        // SELECT * JOIN Cars onto Owner
        try{
            ArrayList<Car> carList = new ArrayList<>();

            PreparedStatement preStmt = dbConn.prepareStatement(
                    "SELECT cars.*,owners.name as 'ownerName' "
                            + "FROM cars JOIN owners ON owners.id = cars.ownerId "
                            + "WHERE owners.id = ?"
            );
            preStmt.setInt(1, ownerId);
            ResultSet resultSet = preStmt.executeQuery();

            while(resultSet.next()){
                int carId = resultSet.getInt("id");
                String makeModel = resultSet.getString("makeModel");
                int prodYear = resultSet.getInt("prodYear");
                String plates = resultSet.getString("plates");
                String ownerName = resultSet.getString("ownerName");
                carList.add(new Car(carId,makeModel,prodYear,plates,ownerId,ownerName));
            }
            return carList;
        }catch (InvalidDataException ex){
            throw new SQLException("Invalid Data From database:INTERNAL ERROR");
        }
    }
    
    void giveUpCar(int carId) throws SQLException{
        PreparedStatement preStmt = dbConn.prepareStatement("UPDATE cars SET ownerId=NULL WHERE id=?");
        preStmt.setInt(1, carId);
        preStmt.executeUpdate();
    }
    
    
    
    
    ArrayList<Car> getAllCars() throws SQLException {
        // return cars and possibly their owner names
        try{
            ArrayList<Car> carList = new ArrayList<>();
            PreparedStatement preStmt = dbConn.prepareStatement(
                    "SELECT cars.*, owners.name as 'ownerName' "
                            + "FROM cars LEFT JOIN owners ON owners.id = cars.ownerId "
                            + "ORDER BY cars.id"
            );
            ResultSet resultSet = preStmt.executeQuery();
            
            while(resultSet.next()){
                int carId = resultSet.getInt("id");
                String makeModel = resultSet.getString("makeModel");
                int prodYear = resultSet.getInt("prodYear");
                String plates = resultSet.getString("plates");
                String ownerName = resultSet.getString("ownerName");
                int ownerId = resultSet.getInt("ownerId");// If ownerId is NULL, 0 return;
                carList.add(new Car(carId,makeModel,prodYear,plates,ownerId,ownerName));
            }
            return carList;
        }catch (InvalidDataException ex){
            throw new SQLException("Invalid Data From database:INTERNAL ERROR");
        }
    }

    void addCar(Car car) throws SQLException, UniqueValueRequiredException {
        // select first to check that plates are not used yet
        // throw UniqeValueRequiredException if check fails
      
          
        String plates = car.getPlates();
        if(plates != null){
            PreparedStatement preStmtCheck = dbConn.prepareStatement("SELECT id FROM cars WHERE plates=?");
            preStmtCheck.setString(1, CONN_STR);
            ResultSet resultSet = preStmtCheck.executeQuery();
            if(resultSet.next()){
                throw new UniqueValueRequiredException(plates + "(plates) is already registered");
            }
        }

        String queryStr = String.format(
                "INSERT INTO cars VALUES (NULL,%s,?,?,?)",
                (car.getOwnerId() == 0)? "NULL" : "?"
            );
        int index = 0;
        PreparedStatement preStmtAdd = dbConn.prepareStatement(queryStr);
        
        int ownerId = car.getOwnerId();
        if(ownerId != 0){
            preStmtAdd.setInt(++index,ownerId);
        }
        preStmtAdd.setString(++index,car.getMakeModel());
        preStmtAdd.setInt(++index, car.getProdYear());
        preStmtAdd.setString(++index, plates);
        preStmtAdd.executeUpdate();
        
    }
//    
//
    void updateCar(Car car) throws SQLException, UniqueValueRequiredException {
        // select first to check that plates are not used yet
        // or that the only one with those plates is car.id
        // throw UniqeValueRequiredException if check fails
        
        String plates = car.getPlates();
        if(plates != null){
            PreparedStatement preStmtCheck = dbConn.prepareStatement("SELECT id FROM cars WHERE plates=?");
            preStmtCheck.setString(1, CONN_STR);
            ResultSet resultSet = preStmtCheck.executeQuery();
            if(resultSet.next()){
                if(resultSet.getInt("id") != car.getId()){
                    throw new UniqueValueRequiredException(plates + "(plates) is already registered");
                }
            }
        }

        PreparedStatement preStmtUpdate = dbConn.prepareStatement(
                "UPDATE cars SET ownerId=?,makeModel=?,prodYear=?,plates=? "
                        + "WHERE id=?"
        );
        int ownerId = car.getOwnerId();
        String ownerIdStr = (ownerId == 0)?null : ownerId + "";
        preStmtUpdate.setString(1,ownerIdStr);
        preStmtUpdate.setString(2,car.getMakeModel());
        preStmtUpdate.setInt(3, car.getProdYear());
        preStmtUpdate.setString(4, plates);
        preStmtUpdate.setInt(5,car.getId());
        preStmtUpdate.executeUpdate();
    }
    

    void deleteCar(int id) throws SQLException {
        PreparedStatement preStmt = dbConn.prepareStatement("DELETE FROM cars WHERE id=?");
        preStmt.setInt(1, id);
        preStmt.executeUpdate();
    }

    
    // generic method
    static <T> void copyToModel(ArrayList<T> list, DefaultListModel<T> model) {
        model.clear();
        for (T item : list) {
            model.addElement(item);
        }
    }
    
}