
package day11carowners;

public class Car {
    public Car(int id, String makeModel,
            int prodYear,String plates,
            int ownerId,String ownerName) throws InvalidDataException{
        setId(id);
        setMakeModel(makeModel);
        setProdYear(prodYear);
        setPlates(plates);
        setOwnerId(ownerId);
        setOwnerName(ownerName);
    }
    private int id;
    private String makeModel;
    private int prodYear;
    private String plates;
    private int ownerId;
    private String ownerName;
    
    public int getId(){ return id;}
    private void setId(int id){ this.id = id;}
    
    public String getMakeModel(){ return makeModel; }
    private void setMakeModel(String makeModel) throws InvalidDataException {
        if(makeModel == null){
            throw new InvalidDataException("Invalid Make model");
        }
        if(makeModel.isEmpty() || makeModel.length() > 100){
            throw new InvalidDataException("Invalid Make model");
        }
        this.makeModel = makeModel;
    }
    
    public int getProdYear() { return this.prodYear; }
    private void setProdYear(int prodYear) throws InvalidDataException {
        if(prodYear > 2100 || prodYear < 1900){
            throw new InvalidDataException("Invalid prodYear");
        }
        this.prodYear = prodYear;
    }
    
    public String getPlates() { return this.plates; }
    private void setPlates(String plates) throws InvalidDataException {
        if(plates == null){
            this.plates = null;
            return;
        }
        if(!plates.matches("[a-zA-Z0-9 ]{6,10}")){
            throw new InvalidDataException("Plate must be 6~10 letters, digits, spaces");
        }
        this.plates = plates;
    }
    
    public int getOwnerId(){ return this.ownerId; }
    private void setOwnerId(int ownerId) { this.ownerId = ownerId; }
    
    public String ownerName(){ return this.ownerName; }
    private void setOwnerName(String ownerName){ this.ownerName = ownerName; }

    @Override
    public String toString(){
        return String.format("%d:%s,%d,%s,%s", 
                id,makeModel,prodYear,
                (ownerName == null)? "No owner":"owner " + ownerName,
                (plates == null)? "No plates":plates
        );
    }
}


