/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day09todosdb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Date;

class InvalidDataException extends Exception {
    InvalidDataException(String msg) { super(msg); }
}
enum Status {
    Pending, Done,Delegated
}

public class Todo {
    private int id;
    private String task; // 1-100 characters, any characters
    private int difficulty; // 1-5
    private LocalDate dueDate; // any valid date where year is 1900 to 2100 both inclusive
    private Status status;

    public Todo(int id, String task, int difficulty, LocalDate dueDate, Status status) throws InvalidDataException {
        setId(id);
        setTask(task);
        setDifficulty(difficulty);
        setDueDate(dueDate);
        setStatus(status);
    }
    
    public Todo(int id, String task, int difficulty, Date dueDate, String status) throws InvalidDataException {
        setId(id);
        setTask(task);
        setDifficulty(difficulty);
        setDueDate(dueDate);
        setStatus(status);
    }
    
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
    
    public int getId(){ return id; }
    public void setId(int id){ this.id = id; }
    public void setId(String id) 
        throws InvalidDataException{
        try{
            setId(Integer.parseInt(id)); //ex
        }catch (NumberFormatException ex){
            throw new InvalidDataException("Id must be numeric");
        }
    }
    
   
    public String getTask(){ return task; }
    public void  setTask(String task) 
            throws InvalidDataException{
        if(task == null || task.isEmpty()){
            throw new InvalidDataException("Task shouldn't be NULL or empty");
        }
        
        if(task.length() > 100){
            throw new InvalidDataException("Task must be 1-100 characters");
        }
        
        this.task = task;
    }
    
    public int getDifficulty(){ return difficulty; }
    public void setDifficulty(int difficulty) 
        throws InvalidDataException{
        if(difficulty > 5 || 1 > difficulty){
            throw new InvalidDataException("Difficulty must be 1-5");
        }
        
        this.difficulty = difficulty;
    }
    public void setDifficulty(String difficulty)
        throws InvalidDataException{
        try{
            setDifficulty(Integer.parseInt(difficulty));
        } catch(NumberFormatException ex){
            throw new InvalidDataException("Difficulty must be 1-5 not string");
        }
        
    }
    
    public LocalDate getDueDate(){ return dueDate; }
    public Date getDueDateAsDate() throws InvalidDataException {
        try{
            return dateFormatter.parse(dueDate.toString());
        } catch (ParseException ex){
            throw new InvalidDataException("Internal Error: dueDate (LocalDate -> date");
        }
    }
    public void setDueDate(LocalDate dueDate) throws InvalidDataException {
        int year = dueDate.getYear();
        if(year > 2100 || year < 1900){
            throw new InvalidDataException("dueDate must be 1900 to 2100 both inclusive");
        }
        
        this.dueDate = dueDate;
    }
    public void setDueDate(Date dueDate) throws InvalidDataException{
        if(dueDate == null){ throw new InvalidDataException("Date cannot be empty"); }
        setDueDate(LocalDate.parse(dateFormatter.format(dueDate)));
    }
    public void setDueDate(String dueDate) throws InvalidDataException{
        try {
            setDueDate(LocalDate.parse(dueDate));
        } catch (DateTimeParseException ex){
            throw new InvalidDataException("Date format is wrong");
        }
    }
    
    public Status getStatus() { return status; }
    public void setStatus(Status status){ this.status = status;}
    public void setStatus(String status) throws InvalidDataException{ 
        try {
            setStatus(Status.valueOf(status));
        } catch (IllegalArgumentException ex) {
            throw new InvalidDataException("Status must be Pending, Done, Delegated");
        }
    }
    
    @Override
    public String toString(){
        return String.format("%d: \"%s\" Due Date: %s, %s",id,task,dueDate.toString(),status.toString());
    }
}
