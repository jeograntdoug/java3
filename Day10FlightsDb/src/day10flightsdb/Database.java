
package day10flightsdb;

import java.util.ArrayList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;


public class Database {
    private Connection dbConn;
    private final String CONN_STR = "jdbc:mysql://localhost/ipd20flights?user=heok&password=lala";
    
    public Database() throws SQLException{
        dbConn = DriverManager.getConnection(CONN_STR);
    }
    
    public ArrayList<Flight> getFlights() throws SQLException{
        try{
            ArrayList<Flight> flightList = new ArrayList<>();
            PreparedStatement preStmt = dbConn.prepareStatement("SELECT * FROM flights");
            ResultSet resultSet = preStmt.executeQuery();

            while(resultSet.next()){
                int id = resultSet.getInt("id");
                String onDayStr = resultSet.getString("onDay");

                String fromCode = resultSet.getString("fromCode");
                String toCode = resultSet.getString("toCode");
                String typeStr = resultSet.getString("type");
                int passengers = resultSet.getInt("passengers");

                flightList.add(
                        new Flight(id,onDayStr,fromCode,toCode,typeStr,passengers)
                );

            }
            return flightList;
        } catch (InvalidDataException ex){
            throw new SQLException("Invalid Data from DB",ex);
        }
    }
    
    public void addFlight(Flight flight) throws SQLException{
        PreparedStatement preStmt = dbConn.prepareStatement("INSERT INTO flights VALUES(NULL,?,?,?,?,?) ");
        preStmt.setDate(1,java.sql.Date.valueOf(flight.getOnDay()));
        preStmt.setString(2,flight.getFromCode());
        preStmt.setString(3,flight.getToCode());
        preStmt.setString(4,flight.getType()+"");
        preStmt.setInt(5,flight.getPassengers());
        
        preStmt.executeUpdate();
    }
    
    public void updateFlight(Flight flight) throws SQLException{
        PreparedStatement preStmt 
                = dbConn.prepareStatement(
                        "UPDATE flights SET "
                                + "onDay=?,"
                                + "fromCode=?,"
                                + "toCode=?,"
                                + "type=?,"
                                + "passengers=? "
                        + "WHERE id=?"
                );
        preStmt.setString(1, flight.getOnDay().toString());
        preStmt.setString(2, flight.getFromCode());
        preStmt.setString(3, flight.getToCode());
        preStmt.setString(4, flight.getType().toString());
        preStmt.setInt(5, flight.getPassengers());
        preStmt.setInt(6,flight.getId());
        
        preStmt.executeUpdate();
    }
}
