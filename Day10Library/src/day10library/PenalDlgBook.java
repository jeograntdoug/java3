
package day10library;

import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;


public class PenalDlgBook extends javax.swing.JPanel {
    private ArrayList<Book> bookList = new ArrayList<>();
    private DefaultListModel<Book> modelBookList = new DefaultListModel<>();
    private Database db;
    /**
     * Creates new form PenalDlgBook
     */
    public PenalDlgBook() {
        initComponents();
        
        try{
            db = DatabaseManager.getDatabaseInstance();
            
        } catch(SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Cannot connect Dababase(ManageBook). Program exit",
                    "DABABASE ERROR",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }
    
    // reset Input fields by select or not
    private void resetInputFields(){
        Book book = lstBook.getSelectedValue();
        if(book == null){
            lblIdVal.setText("-");
            tfIsbn.setText("");
            tfAuthors.setText("");
            tfTitle.setText("");
            spinCopiesTotal.setValue(0);
            comboGenre.setSelectedIndex(-1);
            
           
            btDelete.setEnabled(false);
            btUpdate.setEnabled(false);
            return;
        } 
        
        lblIdVal.setText(book.getId() + "");
        tfIsbn.setText(book.getIsbn());
        tfAuthors.setText(book.getAuthors());
        tfTitle.setText(book.getTitle());
        spinCopiesTotal.setValue(book.getCopiesTotal());
        comboGenre.setSelectedItem(book.getGenre().toString());
        
        btDelete.setEnabled(true);
        btUpdate.setEnabled(true);
        
       
    }
    
    public void reloadBookList(){ 
        try{
            tfSearch.setText("");// clear search text field
            bookList = db.getBookList();
            modelBookList.clear();
            modelBookList.addAll(bookList);
        } catch (SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Cannot read data",
                    "ERROR Data load",
                    JOptionPane.ERROR_MESSAGE
            );
            System.exit(1);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        tfSearch = new javax.swing.JTextField();
        btClear = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstBook = new javax.swing.JList<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblIdVal = new javax.swing.JLabel();
        tfIsbn = new javax.swing.JTextField();
        tfAuthors = new javax.swing.JTextField();
        tfTitle = new javax.swing.JTextField();
        spinCopiesTotal = new javax.swing.JSpinner();
        comboGenre = new javax.swing.JComboBox<>();
        btAdd = new javax.swing.JButton();
        btUpdate = new javax.swing.JButton();
        btDelete = new javax.swing.JButton();

        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Search:");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 14, 45, -1));

        tfSearch.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tfSearchKeyReleased(evt);
            }
        });
        add(tfSearch, new org.netbeans.lib.awtextra.AbsoluteConstraints(59, 11, 189, -1));

        btClear.setText("clear");
        btClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClearActionPerformed(evt);
            }
        });
        add(btClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(266, 10, 66, -1));

        lstBook.setModel(modelBookList);
        lstBook.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        lstBook.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                lstBookValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(lstBook);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 37, 370, 253));

        jLabel2.setText("ID: ");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 37, 39, -1));

        jLabel3.setText("ISBN: ");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 71, 39, -1));

        jLabel4.setText("Authors:");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 108, -1, -1));

        jLabel5.setText("Title: ");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 145, 39, -1));

        jLabel6.setText("Copies Total:");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 182, 67, -1));

        jLabel7.setText("Genre:");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 220, 39, -1));

        lblIdVal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblIdVal.setText("-");
        add(lblIdVal, new org.netbeans.lib.awtextra.AbsoluteConstraints(452, 37, 123, -1));
        add(tfIsbn, new org.netbeans.lib.awtextra.AbsoluteConstraints(447, 68, 154, -1));
        add(tfAuthors, new org.netbeans.lib.awtextra.AbsoluteConstraints(447, 105, 154, -1));
        add(tfTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(447, 142, 154, -1));
        add(spinCopiesTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(465, 179, 43, -1));

        comboGenre.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Fiction", "Novel", "Mystery", "Fantasy", "Self_Help" }));
        add(comboGenre, new org.netbeans.lib.awtextra.AbsoluteConstraints(447, 217, 154, -1));

        btAdd.setText("Add");
        btAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAddActionPerformed(evt);
            }
        });
        add(btAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 269, 65, -1));

        btUpdate.setText("Update");
        btUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btUpdateActionPerformed(evt);
            }
        });
        add(btUpdate, new org.netbeans.lib.awtextra.AbsoluteConstraints(465, 269, -1, -1));

        btDelete.setText("Delete");
        btDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDeleteActionPerformed(evt);
            }
        });
        add(btDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(536, 269, 65, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void tfSearchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSearchKeyReleased
        String searchStr = tfSearch.getText();
        if(searchStr == null) { return; }
        if(searchStr.contains(" ")){ return; }
        
        modelBookList.clear();
        for(Book b : bookList){
            if(b.isMatches(searchStr)){
                modelBookList.addElement(b);
            }
        }
    }//GEN-LAST:event_tfSearchKeyReleased

    private void btClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btClearActionPerformed
        tfSearch.setText("");
        modelBookList.clear();
        modelBookList.addAll(bookList);
    }//GEN-LAST:event_btClearActionPerformed

    private void lstBookValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_lstBookValueChanged
        resetInputFields();
    }//GEN-LAST:event_lstBookValueChanged

    private void btAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAddActionPerformed
        try{
            String isbn = tfIsbn.getText();
            String authors = tfAuthors.getText();
            String title = tfTitle.getText();
            int copiesTotal = (Integer)spinCopiesTotal.getValue();
            String genre = (String)comboGenre.getSelectedItem();

            Book book = new Book(0,isbn,authors,title,copiesTotal,genre,0);
            db.addbook(book);
            reloadBookList();
            resetInputFields();
        } catch (InvalidBookDataException | SQLException ex){
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,
                    "Invalid Data:"+ex.getMessage(),
                    "Data Validation Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btAddActionPerformed

    private void btUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btUpdateActionPerformed
        Book selBook = lstBook.getSelectedValue();
        if(selBook == null){
            JOptionPane.showMessageDialog(this,
                    "Please select one of member",
                    "Update Book Error",
                    JOptionPane.ERROR_MESSAGE
            );
            return;
        }
        
        try{
            selBook.setIsbn(tfIsbn.getText());
            selBook.setAuthors(tfAuthors.getText());
            selBook.setTitle(tfTitle.getText());
            selBook.setCopiesTotal((Integer)spinCopiesTotal.getValue());
            selBook.setGenre((String)comboGenre.getSelectedItem());
            
            
            db.updateBook(selBook);
            reloadBookList();
            resetInputFields();
        } catch (SQLException ex){
            JOptionPane.showMessageDialog(this,
                    "Database connection fail",
                    "Update Book Error",
                    JOptionPane.ERROR_MESSAGE
            );
        } catch (InvalidBookDataException ex){
            JOptionPane.showMessageDialog(this,
                    "Invalid Input:" + ex.getMessage(),
                    "Update Book Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btUpdateActionPerformed

    private void btDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDeleteActionPerformed
        Book selBook = lstBook.getSelectedValue();
        if(selBook == null){
            JOptionPane.showMessageDialog(this,
                    "Please select one of member",
                    "Delete Book Error",
                    JOptionPane.ERROR_MESSAGE
            );
            return;
        }
        
        try{
            db.deleteBook(selBook.getId());
            reloadBookList();
            resetInputFields();
        }  catch (SQLException ex){
            JOptionPane.showMessageDialog(this,
                    "Database connection fail",
                    "Delete Book Error",
                    JOptionPane.ERROR_MESSAGE
            );
        }
    }//GEN-LAST:event_btDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAdd;
    private javax.swing.JButton btClear;
    private javax.swing.JButton btDelete;
    private javax.swing.JButton btUpdate;
    private javax.swing.JComboBox<String> comboGenre;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblIdVal;
    private javax.swing.JList<Book> lstBook;
    private javax.swing.JSpinner spinCopiesTotal;
    private javax.swing.JTextField tfAuthors;
    private javax.swing.JTextField tfIsbn;
    private javax.swing.JTextField tfSearch;
    private javax.swing.JTextField tfTitle;
    // End of variables declaration//GEN-END:variables
}
